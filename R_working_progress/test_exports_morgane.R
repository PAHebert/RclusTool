source("initRclusTool.R")

#fichier CYZ du 19/02/2016
#3 versions de CytoClus

system.time(importSample("data/exports_Morgane/19avril2016_3.6.2.6/listmode_R1_19avril16_flr10_5min_5µls_high 2016-04-19 15u08_New Set 1.csv", file.meta="data/exports_Morgane/19avril2016_3.6.2.6/listmode_R1_19avril16_flr10_5min_5µls_high 2016-04-19 15u08_New Set 1_info.txt", file.profiles="data/exports_Morgane/19avril2016_3.6.2.6/profiles_R1_19avril16_flr10_5min_5µls_high 2016-04-19 15u08_New Set 1.csv", file.RDS="s19_36.RDS", file.config="data/config/ULCO_configuration10.csv", sep=";", dec=",") -> s1904_36)

system.time(importSample("data/exports_Morgane/19avril2016_3.7.15.1/listmode_R1_19avril16_flr10_5min_5µls_high 2016-04-19 15u08_New Set 1.csv", file.meta="data/exports_Morgane/19avril2016_3.7.15.1/listmode_R1_19avril16_flr10_5min_5µls_high 2016-04-19 15u08_New Set 1_info.txt", file.profiles="data/exports_Morgane/19avril2016_3.7.15.1/profiles_R1_19avril16_flr10_5min_5µls_high 2016-04-19 15u08_New Set 1.csv", file.RDS="s19_37.RDS", sep=";", dec=",") -> s1904_37)

system.time(importSample("data/exports_Morgane/19avril2016_4.2.0.2/ListMode_R1_19avril16_flr10_5min_5µls_high 2016-04-19 15u08.csv", file.meta="data/exports_Morgane/19avril2016_4.2.0.2/Info_R1_19avril16_flr10_5min_5µls_high 2016-04-19 15u08.txt", file.profiles="data/exports_Morgane/19avril2016_4.2.0.2/Pulses_R1_19avril16_flr10_5min_5µls_high 2016-04-19 15u08.csv", file.RDS="s19_42.RDS", file.config="data/config/ULCO_configuration10.csv", sep=",", dec=".") -> s1904_42)


#fichier CYZ du 18/04/2014
#3 versions de CytoClus

#72280 data lines
system.time(importSample("data/exports_Morgane/18fevrier2014_3.6.2.6/listmode_R1_180214_flr10_4,5uLs 2014-02-18 17u14_New Set 1.csv", file.meta="data/exports_Morgane/18fevrier2014_3.6.2.6/listmode_R1_180214_flr10_4,5uLs 2014-02-18 17u14_New Set 1_info.txt", file.profiles="data/exports_Morgane/18fevrier2014_3.6.2.6/profiles_R1_180214_flr10_4,5uLs 2014-02-18 17u14_New Set 1.csv", file.RDS="s18_36.RDS", file.config="data/config/ULCO_configuration10.csv", sep=";", dec=",") -> s1804_36)

#77232 data lines
system.time(importSample("data/exports_Morgane/18fevrier2014_3.7.15.1/listmode_R1_180214_flr10_4,5uLs 2014-02-18 17u14_New Set 1.csv", file.meta="data/exports_Morgane/18fevrier2014_3.7.15.1/listmode_R1_180214_flr10_4,5uLs 2014-02-18 17u14_New Set 1_info.txt", file.profiles="data/exports_Morgane/18fevrier2014_3.7.15.1/profiles_R1_180214_flr10_4,5uLs 2014-02-18 17u14_New Set 1.csv", file.RDS="s18_37.RDS", file.config="data/config/ULCO_configuration10.csv", sep=";", dec=",") -> s1804_37)

#77256 data lines
system.time(importSample("data/exports_Morgane/18fevrier2014_4.2.0.2/ListMode_R1_180214_flr10_4,5uLs 2014-02-18 17u14.csv", file.meta="data/exports_Morgane/18fevrier2014_4.2.0.2/Info_R1_180214_flr10_4,5uLs 2014-02-18 17u14.txt", file.profiles="data/exports_Morgane/18fevrier2014_4.2.0.2/Pulses_R1_180214_flr10_4,5uLs 2014-02-18 17u14.csv", file.RDS="s18_42.RDS", file.config="data/config/ULCO_configuration10.csv", sep=",", dec=".")-> s1804_42)

