#.PHONY: all clean

DIR_SRC = .
DIR_OUT = ./build_pkg

MAIN_RMD = $(shell find $(DIR_SRC) -maxdepth 1 -name "*.Rmd")
MAIN_FILE = $(shell basename --suffix=.Rmd -a $(MAIN_RMD) "")
MAIN_MD := $(patsubst %, $(DIR_OUT)/%.md, $(MAIN_FILE))
MAIN_PDF := $(patsubst %, $(DIR_OUT)/%.pdf, $(MAIN_FILE))
SRC_FILE = $(shell find $(DIR_SRC) -not -path '*/\.*')

all: $(MAIN_MD)

$(DIR_OUT)/%.md: $(MAIN_RMD)
	mkdir -p $(DIR_OUT)/
	Rscript -e 'nom<-basename("$<");rmarkdown::render(nom, "md_document", intermediates_dir="$(DIR_OUT)", output_dir="$(DIR_OUT)")'

clean:
	rm -fR $(DIR_OUT)
	rm -fR package/*
	rm RclusTool_*.tar.gz
	rm -fr RclusTool.Rcheck
	rm -fR man




#$(WWW_ROOT)/index.html: $(MAIN_MD) $(SRC_FILE) 
	#mkdocs build --clean --site-dir $(WWW_ROOT)
	#rm -f $(WWW_ROOT)/*.Rmd

