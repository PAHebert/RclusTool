---
title: "Script Package Creation"
output: Script_Package_Creation
---

```{r}
#### SCRIPT PACKAGE CREATION ####

rm(list=ls(all=T))
graphics.off();

# Loading necessary packages
library('devtools')
library('roxygen2')
```

```{r}
# CONSERVER LE DOSSIER 'PACKAGE_DTWUMI_COPY' POUR SE SOUVENIR DE L'ENSEMBLE DES FICHIERS ORIGINAUX NECESSAIRES A LA CREATION DU PACKAGE #
# It must contain at least two subfolder, "functions" and "package".
# The firt one will store all functions files of the package.
# The second one will store packages files which will be created from this present script.

# Defining directory where package's functions are stored
path_main <- "."

# Loading the list of all R functions created in the package
path_functions <- file.path(path_main, "R")
list_functions <- list.files(path = path_functions, pattern = ".R", full.names=T)

# Defining package name
existing.text.DESCRIPTION <- readLines("DESCRIPTION")

package_name <- sub("Package: ", grep("Package:", existing.text.DESCRIPTION, value=T), replacement="") # Here the name of the package
version <- sub("Version: ", grep("Version", existing.text.DESCRIPTION, value=T), replacement="") # Package version

# Defining where new package will be stored
path_package <- file.path(path_main, "package", package_name) # Path of the package folder

# Creating package files
package.skeleton(name = package_name,
                 path = file.path(path_main, "package"),
                 force = T,
                 code_files = list_functions)
```

```{r}
# Malgre l'option "clean=T", roxygenize() ne met pas a jour la doc.
# Pour cela, je supprime les fichiers .Rd generes automatiquement
# lors de la creation de l'arborescence de fichiers.

# Remove all .Rd files in 'man' folder before using 'roxygenize()' function
path_man <- file.path(path_package, "man")
list_man <- list.files(path = path_man, full.names=T)
file.remove(list_man)

# Copy existing text within NAMESPACE file and remove NAMESPACE file
existing.text.NAMESPACE1 <- readLines(file.path(path_package, "NAMESPACE"))
file.remove(file.path(path_package, "NAMESPACE"))
devtools::load_all(path_package)
devtools::document()
```

```{r}
# Updating package documentation
roxygenize(path_package, clean = T)

# Put back deleted lines from NAMESPACE
existing.text.NAMESPACE2 <- readLines(file.path(path_package, "NAMESPACE"))
write(c(existing.text.NAMESPACE1, existing.text.NAMESPACE2), file.path(path_package, "NAMESPACE"))
```

```{r}

# Copy and paste DESCRIPTION file (hand written outside package folder)
file.copy(file.path(path_main, "DESCRIPTION"), path_package, overwrite=T)
##### If a DOI is present in the description file, it must be as this: <DOI:xxxxxxxxxx>
##### Update date in description file !!!

# Copy package general documentation to R folder of the package
#inutile !
file.copy(file.path(path_main, paste(package_name, "-package.Rd", sep="")), file.path(path_package, "man"))

## CITATION
# Copy 'inst' folder
file.copy(file.path(path_main, "inst"), path_package, recursive=TRUE)
# Copy CITATION file to this folder
#inutile !
file.copy(file.path(path_main, "CITATION"), file.path(path_package, "inst"))

```
Test du package
```{r}
# Test
load_all(path_package)

# Build package
system(paste("R CMD build ", path_package, sep=""))

# Check package
system(paste("R CMD check ", package_name, "_", version, ".tar.gz", " --as-cran", sep=""))

# # Install package
# setwd(paste(path_main, "package/", sep = ""))
# install.packages(paste(package_name, "_", version, ".tar.gz", sep = ""),
#                  repos = NULL,
#                  source = "source")
```

