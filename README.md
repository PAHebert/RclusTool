# Welcome to RclusTool [R package]

<a href="http://www-lisic.univ-littoral.fr/"><img src="./logo/logo_lisic.jpg" height=55px></a>

## What is RclusTool ?

RclusTool is a Clustering and visualization toolbox.

## Package content 

- visualization and processing of data with different formats: profile/time series, features and images
- unsupervised clustering
- semi-supervised clustering
- supervised classification
- labeling expert interface
- constraint expert labeling (matching without specific name : points must be link or can not link)

## How to use this R package

From a `R` console:

```{r}
library(RclusTool)
RclusTool()
```

## Contributing organizations

### Development
<a href="http://www-lisic.univ-littoral.fr/"><img src="./logo/logo_lisic.jpg" height=55px></a>
<a href="http://www-univ-littoral.fr/"><img src="./logo_ulco_fonce.jpg" height=55px></a>
<img src="./logo/logo_log.png" height=55px>
<img src="./logo/logo_cnrs.png" height=55px>


- [ULCO](http://univ-littoral.fr)/[LISIC](http://www-lisic.univ-littoral.fr/)

- [LOG](http://log.univ-littoral.fr) 

### Other contributions
<img src="./logo/ifremer.jpg" height=55px>
<img src="./logo/Cefas_logo.png" height=55px>
<img src="./logo/logo_rws.jpg" height=55px>

- [IFREMER/LER-BL](https://wwz.ifremer.fr/manchemerdunord/)

- [CEFAS](http://www.cefas.co.uk)

- [Rijkswaterstaat](http://www.rijkswaterstaat.nl/en/)

## Funding partners
<a href="http://www.univ-littoral.fr/"><img src="./logo/logo_ulco_fonce.jpg" height=55px></a>
<img src="./logo/logo_dymaphy.jpeg" height=55px>
<img src="./logo/logo_cnrs.png" height=55px>
<img src="./logo/MARCO_LOGO.png" height=55px>
<img src="./logo/feder_HdF.jpg" height=55px>
<img src="./logo/region_HdF.png" height=55px>
<img src="./logo/logo_enseignement.jpg" height=55px>
<img src="./logo/logo_ecologie.png" height=55px>
<img src="./logo/logo_2_mers.jpg" height=55px>
<img src="./logo/Logo_jerico.jpg" height=55px>
<img src="./logo/logo_europe.png" height=55px>

---

Contact [us](mailto:hebert@univ-littoral.fr) at hebert[at]univ[-]littoral[dot]fr
